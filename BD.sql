create schema twitter;

use twitter;

create table Tweet(
	tweet_id decimal(20) primary key not null unique,
    content_text varchar(255),
	creation_date datetime , 
    language varchar(255),
    retweet decimal(20),
    geolocation varchar(255),
	user_name varchar(255),
    user_id decimal(20) not null
);